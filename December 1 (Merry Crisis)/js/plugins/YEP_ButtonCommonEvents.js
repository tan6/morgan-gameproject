//=============================================================================
// Yanfly Engine Plugins - Button Common Events
// YEP_ButtonCommonEvents.js
//=============================================================================

var Imported = Imported || {};
Imported.YEP_ButtonCommonEvents = true;

var Yanfly = Yanfly || {};
Yanfly.BCE = Yanfly.BCE || {};
Yanfly.BCE.version = 1.02

//=============================================================================
 /*:
 * @plugindesc v1.02 On the field map, call common events when certain
 * buttons are pressed on the keyboard.
 * @author Yanfly Engine Plugins
 *
 * @param ---Top Row---
 * @default
 *
 * @param Key ~
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 1
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 2
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 3
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 4
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 5
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 6
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 7
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 8
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 9
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key 0
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key -
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key =
 * @parent ---Top Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param ---2nd Row---
 * @default
 *
 * @param Key Q (PageUp)
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key W (PageDown)
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key E
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key R
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key T
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Y
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key U
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key I
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key O
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key P
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key [
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key ]
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key \
 * @parent ---2nd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param ---3rd Row---
 * @default
 *
 * @param Key A
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key S
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key D
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key F
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key G
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key H
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key J
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key K
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key L
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key ;
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key "
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Enter (OK)
 * @parent ---3rd Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param ---4th Row---
 * @default
 *
 * @param Key Shift (Dash)
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Z (OK)
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key X (Cancel)
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key C
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key V
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key B
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key N
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key M
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key ,
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key .
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key /
 * @parent ---4th Row---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param ---Misc---
 * @default
 *
 * @param Key Space (OK)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Left (Left)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Up (Up)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Right (Right)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Down (Down)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Insert (Cancel)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Delete
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Home
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key End
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Page Up (PageUp)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key Page Down (PageDown)
 * @parent ---Misc---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param ---NumPad---
 * @default
 *
 * @param Key NumPad 0 (Cancel)
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 1
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 2 (Down)
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 3
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 4 (Left)
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 5
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 6 (Right)
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 7
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 8 (Up)
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad 9
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad .
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad +
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad -
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad *
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @param Key NumPad /
 * @parent ---NumPad---
 * @type common_event
 * @desc The common event to call when this button is pressed.
 * Set to 0 if you don't wish for a common event to call.
 * @default 0
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin enables you to bind common events to the individual buttons on
 * your keyboard. Instead of having the standard Z for OK and X for cancel,
 * you can make other keys work differently. With the exception of important
 * keys that shouldn't be altered, nearly full access is given across the span
 * of the keyboard.
 *
 * ============================================================================
 * Instructions
 * ============================================================================
 *
 * In the plugin's parameters, you will see a list of all the keys that you can
 * bind to a common event. If that number is something other than 0, then the
 * number associated with it will be the common event that will run. If you
 * assign it to a common event ID that does not exist, you will get an error so
 * please be wary of that.
 *
 * You may also notice that some of the keys have in parenthesis a word like
 * (OK) or (Cancel) next to them. What this means is that those keys already
 * have a function assigned to them by the game. If you assign a common event
 * to these keys, the native function of the key will be removed in favor of
 * the common event you've assigned.
 *
 * Here is a list of the keys that already have a common assigned:
 *
 * Key - What they're assigned to
 *   - Q         - Assigned to PageUp
 *   - W         - Assigned to PageDown
 *   - Shift     - Assigned to Dash
 *   - Z         - Assigned to OK
 *   - X         - Assigned to Cancel
 *   - Space     - Assigned to OK
 *   - Left      - Assigned to moving left
 *   - Up        - Assigned to moving up
 *   - Right     - Assigned to moving right
 *   - Down      - Assigned to moving down
 *   - Insert    - Assigned to Cancel
 *   - Page Up   - Assigned to PageUp
 *   - Page Down - Assigned to PageDown
 *   - Numpad 0  - Assigned to Cancel
 *   - Numpad 2  - Assigned to moving down
 *   - Numpad 4  - Assigned to moving left
 *   - Numpad 6  - Assigned to moving right
 *   - Numpad 8  - Assigned to moving up
 *
 * Once again, if you assign common events to these keys, the common event will
 * removing the binding the key had natively. However, this will only apply
 * while the player is in the field map. Being inside of a menu or battle
 * system will restore the previously native functions.
 *
 * ============================================================================
 * Compatibility Issues
 * ============================================================================
 *
 * This plugin will most likely have compatibility issues with anything that
 * alters keystrokes or makes use of them through a different manner.
 *
 * This will include the KeyboardConfig.js that was provided for the RPG Maker
 * MV plugin pack made by Yanfly Engine Plugins. A revision of this plugin
 * KeyboardConfig.js is made on Yanfly.moe for you to pick up! Make sure you
 * have YEP_KeyboardConfig.js version 1.01 in order for this to be compatible
 * with it. This plugin must be placed above YEP_KeyboardConfig.js for the
 * two plugins to work together.
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * For those who would like for a way to toggle back and forth between the
 * bound common events and the default buttons, use these plugin commands.
 *
 * Plugin Commands
 *
 *   RevertButton Ok
 *   RevertButton Cancel
 *   RevertButton Dash
 *   RevertButton PageUp
 *   RevertButton PageDown
 *   RevertButton Left
 *   RevertButton Up
 *   RevertButton Right
 *   RevertButton Down
 *   RevertButton All
 *   - Reverts all keys bound to any of the original functions back to their
 *   original buttons and unbinds the common events bound to them. If the "All"
 *   function is reverted, then all affected buttons will revert back to their
 *   original functions.
 *
 *   SwitchButton Ok
 *   SwitchButton Cancel
 *   SwitchButton Dash
 *   SwitchButton PageUp
 *   SwitchButton PageDown
 *   SwitchButton Left
 *   SwitchButton Up
 *   SwitchButton Right
 *   SwitchButton Down
 *   SwitchButton All
 *   - Switches all keys with original functions to use the common event binds
 *   instead of their original versions. If the "All" function is switched,
 *   then all affected buttons will switch to common event bindings if there
 *   are any.
 *
 *   TriggerButton Ok
 *   TriggerButton Cancel
 *   TriggerButton Dash
 *   TriggerButton PageUp
 *   TriggerButton PageDown
 *   TriggerButton Left
 *   TriggerButton Up
 *   TriggerButton Right
 *   TriggerButton Down
 *   - This will cause the game to simulate triggering the button command of
 *   one of those original functions even if there is a common event bound to
 *   all of the keys of that original function.
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.02:
 * - Updated for RPG Maker MV version 1.5.0.
 *
 * Version 1.01:
 * - Changed buttons from triggering to repeating so that common events can
 * continuously run while being held down.
 *
 * Version 1.00:
 * - Finished Plugin!
 */
//=============================================================================

//=============================================================================
// Parameter Variables
//=============================================================================

Yanfly.Parameters = PluginManager.parameters('YEP_ButtonCommonEvents');
Yanfly.Param = Yanfly.Param || {};

Yanfly.Param.BCEList = {
          1: Number(Yanfly.Parameters['Key 1']),
          2: Number(Yanfly.Parameters['Key 2']),
          3: Number(Yanfly.Parameters['Key 3']),
          4: Number(Yanfly.Parameters['Key 4']),
          5: Number(Yanfly.Parameters['Key 5']),
          6: Number(Yanfly.Parameters['Key 6']),

          w: Number(Yanfly.Parameters['Key W ']),
          a: Number(Yanfly.Parameters['Key A']),
          s: Number(Yanfly.Parameters['Key S']),
          d: Number(Yanfly.Parameters['Key D']),
      enter: Number(Yanfly.Parameters['Key Enter (OK)']),
      keyShift: Number(Yanfly.Parameters['Key Shift (Dash)']),
      space: Number(Yanfly.Parameters['Key Space (OK)']),
    dirLeft: Number(Yanfly.Parameters['Key Left (Left)']),
      dirUp: Number(Yanfly.Parameters['Key Up (Up)']),
   dirRight: Number(Yanfly.Parameters['Key Right (Right)']),
    dirDown: Number(Yanfly.Parameters['Key Down (Down)']),
};
Yanfly.Param.Variables = String(Yanfly.Parameters['Variables']);

//=============================================================================
// Input Key Mapper
//=============================================================================

if (Yanfly.Param.BCEList['1'] !== 0) Input.keyMapper[49]          = '1';
if (Yanfly.Param.BCEList['2'] !== 0) Input.keyMapper[50]          = '2';
if (Yanfly.Param.BCEList['3'] !== 0) Input.keyMapper[51]          = '3';
if (Yanfly.Param.BCEList['4'] !== 0) Input.keyMapper[52]          = '4';
if (Yanfly.Param.BCEList['5'] !== 0) Input.keyMapper[53]          = '5';
if (Yanfly.Param.BCEList['6'] !== 0) Input.keyMapper[54]          = '6';

if (Yanfly.Param.BCEList['w'] !== 0) Input.keyMapper[87]          = 'w';
if (Yanfly.Param.BCEList['a'] !== 0) Input.keyMapper[65]          = 'a';
if (Yanfly.Param.BCEList['s'] !== 0) Input.keyMapper[83]          = 's';
if (Yanfly.Param.BCEList['d'] !== 0) Input.keyMapper[68]          = 'd';
if (Yanfly.Param.BCEList['enter'] !== 0) Input.keyMapper[13]      = 'enter';

if (Yanfly.Param.BCEList['keyShift'] !== 0) Input.keyMapper[16]   = 'keyShift';
if (Yanfly.Param.BCEList['space'] !== 0) Input.keyMapper[32]     = 'space';
if (Yanfly.Param.BCEList['dirLeft'] !== 0) Input.keyMapper[37]   = 'dirLeft';
if (Yanfly.Param.BCEList['dirUp'] !== 0) Input.keyMapper[38]     = 'dirUp';
if (Yanfly.Param.BCEList['dirRight'] !== 0) Input.keyMapper[39]  = 'dirRight';
if (Yanfly.Param.BCEList['dirDown'] !== 0) Input.keyMapper[40]   = 'dirDown';

//=============================================================================
// Input
//=============================================================================

Input._revertButton = function(button) {
  if (button === 'OK') {
    this.keyMapper[13] = 'ok';
    this.keyMapper[32] = 'ok';
  } else if (button === 'CANCEL') {
    this.keyMapper[27] = 'escape';
  } else if (button === 'DASH') {
    this.keyMapper[16] = 'shift';
  } else if (button === 'LEFT') {
    this.keyMapper[37] = 'left';
    this.keyMapper[65] = 'left';
  } else if (button === 'UP') {
    this.keyMapper[38] = 'up';
    this.keyMapper[87] = 'up';
  } else if (button === 'RIGHT') {
    this.keyMapper[39] = 'right';
    this.keyMapper[68] = 'right';
  } else if (button === 'DOWN') {
    this.keyMapper[40] = 'down';
    this.keyMapper[83] = 'down';
  } else if (button === 'ALL') {
    this.keyMapper[13] = 'ok';
    this.keyMapper[32] = 'ok';
    this.keyMapper[37] = 'left';
    this.keyMapper[38] = 'up';
    this.keyMapper[39] = 'right';
    this.keyMapper[40] = 'down';

  }
}

Input._switchButton = function(button) {
  if (button === 'OK') {
    if (Yanfly.Param.BCEList['enter'] !== 0) this.keyMapper[13] = 'enter';
    if (Yanfly.Param.BCEList['space'] !== 0) this.keyMapper[32] = 'space';
  } else if (button === 'DASH') {
    if (Yanfly.Param.BCEList['keyShift'] !== 0) this.keyMapper[16] = 'keyShift';
  } else if (button === 'LEFT') {
    if (Yanfly.Param.BCEList['dirLeft'] !== 0) this.keyMapper[37] = 'dirLeft';
    if (Yanfly.Param.BCEList['a'] !== 0) this.keyMapper[65] = 'a';
  } else if (button === 'UP') {
    if (Yanfly.Param.BCEList['dirUp'] !== 0) this.keyMapper[38] = 'dirUp';
    if (Yanfly.Param.BCEList['w'] !== 0) this.keyMapper[87] = 'w';
  } else if (button === 'RIGHT') {
    if (Yanfly.Param.BCEList['dirRight'] !== 0) this.keyMapper[39] = 'dirRight';
    if (Yanfly.Param.BCEList['d'] !== 0) this.keyMapper[68] = 'd';
  } else if (button === 'DOWN') {
    if (Yanfly.Param.BCEList['dirDown'] !== 0) this.keyMapper[40] = 'dirDown';
    if (Yanfly.Param.BCEList['s'] !== 0) this.keyMapper[83] = 's';
  } else if (button === 'ALL') {
    if (Yanfly.Param.BCEList['enter'] !== 0) this.keyMapper[13] = 'enter';
    if (Yanfly.Param.BCEList['space'] !== 0) this.keyMapper[32] = 'space';
    if (Yanfly.Param.BCEList['keyShift'] !== 0) this.keyMapper[16] = 'keyShift';
    if (Yanfly.Param.BCEList['w'] !== 0) this.keyMapper[82] = 'w';
    if (Yanfly.Param.BCEList['dirLeft'] !== 0) this.keyMapper[37] = 'dirLeft';
    if (Yanfly.Param.BCEList['dirUp'] !== 0) this.keyMapper[38] = 'dirUp';
    if (Yanfly.Param.BCEList['dirRight'] !== 0) this.keyMapper[39] = 'dirRight';
    if (Yanfly.Param.BCEList['dirDown'] !== 0) this.keyMapper[40] = 'dirDown';
  }
};

//=============================================================================
// Scene_Base
//=============================================================================

Yanfly.BCE.Scene_Base_start = Scene_Base.prototype.start;
Scene_Base.prototype.start = function() {
    Yanfly.BCE.Scene_Base_start.call(this);
    Input._revertButton('ALL');
};

//=============================================================================
// Scene_Map
//=============================================================================

Yanfly.BCE.Scene_Map_start = Scene_Map.prototype.start;
Scene_Map.prototype.start = function() {
    Yanfly.BCE.Scene_Map_start.call(this);
    Input._switchButton('ALL');
};

Yanfly.BCE.Scene_Map_updateScene = Scene_Map.prototype.updateScene;
Scene_Map.prototype.updateScene = function() {
    Yanfly.BCE.Scene_Map_updateScene.call(this);
    if (SceneManager.isSceneChanging()) return;
    if ($gameMap.isEventRunning()) return;
    this.updateButtonEvents();
};

Scene_Map.prototype.updateButtonEvents = function() {
    for (var key in Yanfly.Param.BCEList) {
      var eventId = Yanfly.Param.BCEList[key];
      if (eventId <= 0) continue;
      if (!Input.isRepeated(key)) continue;
      $gameTemp.reserveCommonEvent(eventId);
      break;
    }
};

//=============================================================================
// Game_Interpreter
//=============================================================================

Yanfly.BCE.Game_Interpreter_pluginCommand =
    Game_Interpreter.prototype.pluginCommand;
Game_Interpreter.prototype.pluginCommand = function(command, args) {
  Yanfly.BCE.Game_Interpreter_pluginCommand.call(this, command, args);
  if (command === 'RevertButton') this.revertButton(args);
  if (command === 'SwitchButton') this.switchButton(args);
  if (command === 'TriggerButton') this.triggerButton(args);
};

Game_Interpreter.prototype.revertButton = function(args) {
  if (!args) return;
  var button = args[0].toUpperCase();
  Input._revertButton(button);
};

Game_Interpreter.prototype.switchButton = function(args) {
  if (!args) return;
  var button = args[0].toUpperCase();
  Input._switchButton(button);
};

Game_Interpreter.prototype.triggerButton = function(args) {
  if (!args) return;
  var button = args[0].toLowerCase();
  if (button === 'cancel') button = 'escape';
  Input._latestButton = button;
  Input._pressedTime = 0;
};

//=============================================================================
// End of File
//=============================================================================
